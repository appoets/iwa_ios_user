//
//  SignUpControllers.swift
//  GoJekUser
//
//  Created by Santhosh on 02/09/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation
import UIKit

class SignUpControllers : UIViewController{
    
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var stckImg1View: UIView!
    
    @IBOutlet weak var stckImg2View: UIView!
    
    @IBOutlet weak var stckImg3View: UIView!
    override func viewWillAppear(_ animated: Bool) {
        self.view.applyGradient(isVertical: true, colorArray: [#colorLiteral(red: 0.1960784314, green: 0.8235294118, blue: 0.7921568627, alpha: 1), #colorLiteral(red: 0.4588235294, green: 0.4196078431, blue: 0.9450980392, alpha: 1)])
    }
    
    override func viewDidLoad() {
        iconView.cornerRadius = 15
        stckImg1View.cornerRadius = stckImg1View.frame.height/2
        stckImg2View.cornerRadius = stckImg1View.frame.height/2
        stckImg3View.cornerRadius = stckImg1View.frame.height/2
    }

}


   


extension UIView {

    func applyGradient(isVertical: Bool, colorArray: [UIColor]) {
        layer.sublayers?.filter({ $0 is CAGradientLayer }).forEach({ $0.removeFromSuperlayer() })
         
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colorArray.map({ $0.cgColor })
        if isVertical {
            //top to bottom
            gradientLayer.locations = [0.0, 1.0]
        } else {
            //left to right
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.4)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        }
        
        backgroundColor = .clear
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }

}


